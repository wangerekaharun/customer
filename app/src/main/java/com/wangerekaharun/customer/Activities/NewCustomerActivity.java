package com.wangerekaharun.customer.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wangerekaharun.customer.HomeActivity;
import com.wangerekaharun.customer.Models.CustomerDetails;
import com.wangerekaharun.customer.R;
import com.wangerekaharun.customer.SQLite.DBOperations;

public class NewCustomerActivity extends AppCompatActivity {
    EditText customerName,address1,address2,state,city,zip;
    Button saveCustomerDetails;
    DBOperations dbOperations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        customerName=(EditText)findViewById(R.id.fullname_edit);
        address1=(EditText)findViewById(R.id.address1_edit);
        address2=(EditText)findViewById(R.id.address2_edit);
        state=(EditText)findViewById(R.id.state_edit);
        city=(EditText) findViewById(R.id.city_edit);
        zip=(EditText)findViewById(R.id.zip_edit);
        saveCustomerDetails=(Button)findViewById(R.id.btn_savedetails);
        dbOperations=new DBOperations(getApplicationContext());


        saveCustomerDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name =customerName.getText().toString().trim();
                String addressone=address1.getText().toString().trim();
                String addresstwo= address2.getText().toString().trim();
                String statee=state.getText().toString().trim();
                String cityy=city.getText().toString().trim();
                String zipp=zip.getText().toString().trim();

                if (!name.isEmpty() && !addressone.isEmpty() && !addresstwo.isEmpty() && !statee.isEmpty() && !cityy.isEmpty() && !zipp.isEmpty()){
                    saveCustomerCredentials(name,addressone,addresstwo,statee,cityy,zipp);
                }else {

                    Toast.makeText(getApplicationContext(),"Please fill in the details",Toast.LENGTH_SHORT).show();
                }

            }

            private void saveCustomerCredentials(String name, String addressone, String addresstwo, String statee, String cityy, String zipp) {
                CustomerDetails customerDetails= new CustomerDetails();
                customerDetails.setCustomerName(name);
                customerDetails.setAddress1(addressone);
                customerDetails.setAdrress2(addresstwo);
                customerDetails.setState(statee);
                customerDetails.setCity(cityy);
                customerDetails.setZip(zipp);

                boolean insertCustomerDetails=dbOperations.insertCustomerDetails(customerDetails);

                if (insertCustomerDetails==true){
                    Toast.makeText(getApplicationContext(),"Customer Added",Toast.LENGTH_SHORT).show();
                    redirect();
                }else {
                    Toast.makeText(getApplicationContext(),"There was a problem adding the customer details",Toast.LENGTH_SHORT).show();
                }
            }

            private void redirect() {
                Intent intent = new Intent(NewCustomerActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

    }


}
