package com.wangerekaharun.customer.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wangerekaharun.customer.HomeActivity;
import com.wangerekaharun.customer.Models.CustomerDetails;
import com.wangerekaharun.customer.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harun on 7/24/17.
 */

public class CustomerDetailsAdapter extends RecyclerView.Adapter<CustomerDetailsAdapter.MyViewHolder> {
    private List<CustomerDetails> customerDetailsList;
    Cursor mCursor;


    public CustomerDetailsAdapter(Context applicationContext, List<CustomerDetails> customerDetailsList) {
        this.customerDetailsList=customerDetailsList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView customerName;

        public MyViewHolder(View itemView) {
            super(itemView);
            customerName=(TextView)itemView.findViewById(R.id.customerNameText);
        }
    }
    @Override
    public CustomerDetailsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.customer_details, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(CustomerDetailsAdapter.MyViewHolder holder, int position) {
        CustomerDetails customerDetails= customerDetailsList.get(position);
        holder.customerName.setText(customerDetails.getCustomerName());

    }

    @Override
    public int getItemCount() {
        return customerDetailsList.size();
    }
    public void setFilter(List<CustomerDetails> customerDetailsList1) {
        customerDetailsList = new ArrayList<>();
        customerDetailsList.addAll(customerDetailsList1);
        notifyDataSetChanged();
    }
}
