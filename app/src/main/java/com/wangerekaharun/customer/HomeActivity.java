package com.wangerekaharun.customer;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wangerekaharun.customer.Activities.NewCustomerActivity;
import com.wangerekaharun.customer.Adapters.CustomerDetailsAdapter;
import com.wangerekaharun.customer.Models.CustomerDetails;
import com.wangerekaharun.customer.SQLite.DBOperations;
import com.wangerekaharun.customer.Utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    RecyclerView recycleview;
    DBOperations dbOperations;
    LinearLayout emptyView;
    Cursor cursor;
    CustomerDetailsAdapter customerDetailsAdapter;
    CustomerDetailsAdapter customerDetailsAdapter1;
    List<CustomerDetails> customerDetailsList;
    static RecyclerView.LayoutManager mLayoutManager;
    Button back;
    TextView fullName,address,city,zip;
    SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //searchView=(SearchView)findViewById(R.id.searchview);





        recycleview=(RecyclerView)findViewById(R.id.customerDetailRv);
        emptyView=(LinearLayout)findViewById(R.id.empty_view);



        initView();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                Intent intent = new Intent(HomeActivity.this, NewCustomerActivity.class);
                startActivity(intent);
            }
        });
    }

     void initView() {
        final DBOperations dbOperations=new DBOperations(getApplicationContext());
        customerDetailsList=dbOperations.getAllCustomers();
        try {
            if (customerDetailsList.isEmpty() || customerDetailsList.equals(null)){
                recycleview.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(),"No customer Details Added",Toast.LENGTH_SHORT).show();
                emptyView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(HomeActivity.this,NewCustomerActivity.class);
                        startActivity(intent);

                    }
                });

            }else {
                recycleview.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
                customerDetailsAdapter=new CustomerDetailsAdapter(getApplicationContext(),customerDetailsList);
                customerDetailsAdapter.notifyDataSetChanged();

                mLayoutManager= new LinearLayoutManager(getApplicationContext());
                recycleview.setLayoutManager(mLayoutManager);
                recycleview.setItemAnimator(new DefaultItemAnimator());
                recycleview.setAdapter(customerDetailsAdapter);
                customerDetailsAdapter.notifyDataSetChanged();
                recycleview.addOnItemTouchListener(new ItemClickListener(getApplicationContext(), recycleview, new ItemClickListener.ClickListener() {
                    @Override
                    public void onClick(View view, final int position) {
                        String addresss=customerDetailsList.get(position).getAddress1()+ ","+customerDetailsList.get(position).getAdrress2();
                        String stateee= customerDetailsList.get(position).getCity()+ ","+customerDetailsList.get(position).getState();


                        final Dialog dialog= new Dialog(HomeActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.customer_dialog_details);
                        back=(Button)dialog.findViewById(R.id.btn_back);
                        fullName=(TextView)dialog.findViewById(R.id.text_fullname);
                        address=(TextView)dialog.findViewById(R.id.text_address);
                        zip=(TextView)dialog.findViewById(R.id.text_zip);
                        city=(TextView)dialog.findViewById(R.id.text_city_state);
                        fullName.setText(customerDetailsList.get(position).getCustomerName());
                        address.setText(addresss);
                        city.setText(stateee);
                        zip.setText(customerDetailsList.get(position).getZip());

                        back.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.cancel();
                            }
                        });
                        dialog.show();

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));


            }
        }catch (Exception a){
            a.printStackTrace();
            recycleview.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(),"No customer Details Added",Toast.LENGTH_SHORT).show();
            emptyView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this,NewCustomerActivity.class);
                    startActivity(intent);
                }
            });
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        customerDetailsAdapter.setFilter(customerDetailsList);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    return  true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<CustomerDetails> filteredModelList = filter(customerDetailsList, newText);

        customerDetailsAdapter.setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<CustomerDetails> filter(List<CustomerDetails> models, String query) {
        query = query.toLowerCase();final List<CustomerDetails> filteredModelList = new ArrayList<>();
        for (CustomerDetails model : models) {
            final String text = model.getCustomerName().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
