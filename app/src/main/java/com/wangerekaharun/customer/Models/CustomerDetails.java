package com.wangerekaharun.customer.Models;

/**
 * Created by harun on 7/24/17.
 */

public class CustomerDetails {
    private String customerName,address1,adrress2,city,state,zip;
    int id;

    public CustomerDetails(){

    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAdrress2() {
        return adrress2;
    }

    public void setAdrress2(String adrress2) {
        this.adrress2 = adrress2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CustomerDetails(String customerName, String address1, String adrress2, String city, String state, String zip, int id){
        this.customerName=customerName;
        this.address1=address1;
        this.adrress2=adrress2;
        this.city=city;
        this.state=state;
        this.zip=zip;
        this.id=id;

    }
}
