package com.wangerekaharun.customer.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by harun on 7/24/17.
 */

public class DBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION =1;

    // Database Name
    private static final String DATABASE_NAME = "customerdetails.db";


    // customer details Table
    String CREATE_TABLE_CUSTOMER_DETAILS = "CREATE TABLE " + DBKeys.CUSTOMER_TABLE+ "("
            + DBKeys.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"

            + DBKeys.KEY_FULLNAME + " TEXT, "

            + DBKeys.KEY_ADRRESS1+ " TEXT, "

            + DBKeys.KEY_ADDRESS2+ " TEXT ,"

            + DBKeys.KEY_CITY + " TEXT,"

            + DBKeys.KEY_STATE + " TEXT,"

            + DBKeys.KEY_ZIP + " TEXT"

            + ")";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CUSTOMER_DETAILS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DBKeys.CUSTOMER_TABLE);

        //CREATE DB AGAIN
        onCreate(db);

    }
}
