package com.wangerekaharun.customer.SQLite;

/**
 * Created by harun on 7/24/17.
 */

public class DBKeys {


    //Customer Details Table Contents
    public static final String CUSTOMER_TABLE="customer_details";

    //Customer Details Table Column Names
    public static final String KEY_ID="id";
    public static final String KEY_FULLNAME="fullname";
    public static final String KEY_ADRRESS1="address1";
    public static final String KEY_ADDRESS2="address2";
    public static final String KEY_CITY="city";
    public static final String KEY_STATE="state";
    public static final String KEY_ZIP="zip";
}
