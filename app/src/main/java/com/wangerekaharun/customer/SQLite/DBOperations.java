package com.wangerekaharun.customer.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.wangerekaharun.customer.Models.CustomerDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harun on 7/24/17.
 */

public class DBOperations {
    private DBHandler dbHandler;
    private static final String TAG = DBOperations.class.getSimpleName();

    public DBOperations(Context context) {
        dbHandler = new DBHandler(context);}

    //CRUD OPERATIONS FOR CUSTOMER DETAILS

    //insert the values

    public boolean insertCustomerDetails(CustomerDetails customerDetails) {
        boolean success = false;

        SQLiteDatabase db = dbHandler.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DBKeys.KEY_FULLNAME, customerDetails.getCustomerName());
        values.put(DBKeys.KEY_ADRRESS1, customerDetails.getAddress1());
        values.put(DBKeys.KEY_ADDRESS2, customerDetails.getAdrress2());
        values.put(DBKeys.KEY_CITY,customerDetails.getCity());
        values.put(DBKeys.KEY_STATE, customerDetails.getState());
        values.put(DBKeys.KEY_ZIP,customerDetails.getZip());


        try {


            // Inserting Row
            if (db.insert(DBKeys.CUSTOMER_TABLE, null, values) >= 1) {
                success = true;
            }


        }
        catch (Exception mm){
            mm.printStackTrace();
        }

        db.close();

        return success;

    }

    //read the values in the db

    public List<CustomerDetails> getAllCustomers() {
        //Open connection to read only
        SQLiteDatabase db = dbHandler.getReadableDatabase();


        List<CustomerDetails> customerDetailsList = new ArrayList<>();
        String QUERY = "SELECT * FROM '" + DBKeys.CUSTOMER_TABLE +"' ORDER BY id DESC";

        Cursor cursor = db.rawQuery(QUERY, null);

        if (!cursor.isLast()) {

            while (cursor.moveToNext()) {
                CustomerDetails   allCustomers = new CustomerDetails();

                allCustomers.setId(cursor.getInt(0));
                allCustomers.setCustomerName(cursor.getString(1));
                allCustomers.setAddress1(cursor.getString(2));
                allCustomers.setAdrress2(cursor.getString(3));
                allCustomers.setCity(cursor.getString(4));
                allCustomers.setState(cursor.getString(5));
                allCustomers.setZip(cursor.getString(6));


                customerDetailsList.add(allCustomers);

            }
        }
        db.close();
        // looping through all rows and adding to list

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return customerDetailsList;


    }
    //deleting a single customer

    public boolean deleteCustomer(String rowId) {
        SQLiteDatabase db = dbHandler.getWritableDatabase();
        return db.delete(DBKeys.CUSTOMER_TABLE, DBKeys.KEY_ID + "= '" + rowId + "' ", null) > 0;
    }

    //counting all customers

    public int countCustomers() {

        SQLiteDatabase db = dbHandler.getWritableDatabase();

        String sql = "SELECT * FROM customer_details" ;
        int recordCount = db.rawQuery(sql, null).getCount();
        db.close();

        return recordCount;

    }

    //search a particular customer
    public Cursor retrieve(String searchTerm)
    {
        SQLiteDatabase db = dbHandler.getWritableDatabase();
        String[] columns={DBKeys.KEY_ID,DBKeys.KEY_FULLNAME};
        Cursor c=null;
        if(searchTerm != null && searchTerm.length()>0)
        {
            String sql="SELECT * FROM "+DBKeys.CUSTOMER_TABLE+" WHERE "+DBKeys.KEY_FULLNAME+" LIKE '%"+searchTerm+"%'";
            c=db.rawQuery(sql,null);
            return c;
        }
        c=db.query(DBKeys.KEY_FULLNAME,columns,null,null,null,null,null);
        return c;
    }



}
